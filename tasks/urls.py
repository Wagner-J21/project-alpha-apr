from django.urls import path
from .views import create_task, my_list_tasks


urlpatterns = [
    path("mine/", my_list_tasks, name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
]
